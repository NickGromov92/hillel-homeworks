let dash = 'dash';
let number = 'number';
let current = 'current';

function getParagraph(part, section, style) {
	let partIns = document.querySelector(`#tab-${part}`);
	let sectionIns = partIns.querySelectorAll('.list__item')[section - 1];
	let arrSection = sectionIns.querySelectorAll('a.list-sub__link');
	let resultText = [];
	let index = 1;
	if (style === current) {
		arrSection.forEach(function (item) {
			resultText.push(`${section}.${index++} ${item.textContent}`)
		})
	} else if (style === number) {
		arrSection.forEach(function (item) {
			resultText.push(`${index++}. ${item.textContent}`)
		})
	} else if (style === dash) {
		arrSection.forEach(function (item) {
			resultText.push(`- ${item.textContent}`)
		})
	}
	console.table(resultText);
}

getParagraf(1, 3);